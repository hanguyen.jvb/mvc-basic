<?php
    class App
    {
        protected $controller = 'LoginController';
        protected $action = 'index';
        protected $param = [];

        function __construct()
        {
            $arrUrl = $this->urlProcess();

            //process controller
            if (file_exists("./mvc/controllers/" . $arrUrl[0] . ".php")) {
                $this->controller = $arrUrl[0];
                unset($arrUrl[0]);
            }

            require_once('./mvc/controllers/' . $this->controller . '.php');
            $this->controller = new $this->controller;

            //process action
            if (isset($arrUrl[1])) {
                if (method_exists($this->controller, $arrUrl[1])) {
                    $this->action = $arrUrl[1];
                }
                unset($arrUrl[1]);
            }

            //process param
            $this->param = $arrUrl ? array_values($arrUrl) : [];
            call_user_func_array([$this->controller, $this->action], $this->param);
        }

        public function urlProcess()
        {
            if (isset($_GET['url'])) {
                return explode('/', filter_var(trim($_GET['url'], "/")));
            }
        }
    }
?>
