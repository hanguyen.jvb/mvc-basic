<?php
    class Student extends Database
    {
        public function listLimitStudent($offset, $recordPerPage)
        {
            $sql = "SELECT * FROM dtb_students
            INNER JOIN dtb_class ON dtb_students.class_id = dtb_class.class_id
            INNER JOIN dtb_department ON dtb_students.department_id = dtb_department.department_id
            ORDER BY student_id DESC LIMIT $offset, $recordPerPage";
            return mysqli_query($this->conn, $sql);
        }

        public function listStudent()
        {
            $sql = "SELECT * FROM dtb_students
            INNER JOIN dtb_class ON dtb_students.class_id = dtb_class.class_id
            INNER JOIN dtb_department ON dtb_students.department_id = dtb_department.department_id
            ORDER BY student_id DESC";
            return mysqli_query($this->conn, $sql);
        }

        public function addStudent($fullname, $email, $phone, $birthday, $gender, $classOfStudent, $departmentOfStudent, $password, $username, $avatar)
        {
            $sql = "INSERT INTO dtb_students(full_name, email, phone, birthday, gender, class_id, department_id, password, username, avatar)
            VALUES ('$fullname', '$email', '$phone', '$birthday', '$gender', '$classOfStudent', '$departmentOfStudent', '$password', '$username', '$avatar')";
            return mysqli_query($this->conn, $sql);
        }

        public function getOneStudent($student_id)
        {
            $sql = "SELECT * FROM dtb_students 
            INNER JOIN dtb_class ON dtb_students.class_id = dtb_class.class_id
            INNER JOIN dtb_department ON dtb_students.department_id = dtb_department.department_id
            WHERE student_id = '$student_id'";
            return mysqli_query($this->conn, $sql);
        }

        public function editStudent($fullname, $email, $phone, $birthday, $gender, $classOfStudent, $departmentOfStudent, $password, $username, $avatarName, $student_id)
        {
            $sql = "UPDATE dtb_students 
            SET full_name = '$fullname', email = '$email', phone = '$phone', birthday = '$birthday', gender = '$gender', 
                class_id = '$classOfStudent', department_id = '$departmentOfStudent', password = '$password', username = '$username', avatar = '$avatarName'
            WHERE student_id = '$student_id'";
            return mysqli_query($this->conn, $sql);
        }

        public function deleteStudent($student_id)
        {
            $sql = "DELETE FROM dtb_students WHERE student_id = '$student_id'";
            return mysqli_query($this->conn, $sql);
        }

        public function searchStudent($keySearch)
        {
            $sql = "SELECT * FROM dtb_students
            INNER JOIN dtb_class ON dtb_students.class_id = dtb_class.class_id
            INNER JOIN dtb_department ON dtb_students.department_id = dtb_department.department_id
            WHERE username LIKE '%$keySearch%' OR full_name LIKE '%$keySearch%' 
            OR class_name LIKE '%$keySearch%' OR department_name LIKE '%$keySearch%'
            OR birthday LIKE '%$keySearch%'";
            return mysqli_query($this->conn, $sql);
        }
    }
?>
