<?php
    class ClassStudent extends Database
    {
        public function listClassOfStudent()
        {
            $sql = "SELECT * FROM dtb_class INNER JOIN dtb_department ON dtb_class.department_id = dtb_department.department_id";
            return mysqli_query($this->conn, $sql);
        }

        public function listClassByDepartment($departmentId)
        {
            $sql = "SELECT * FROM dtb_class WHERE department_id = '$departmentId'";
            return mysqli_query($this->conn, $sql);
        }
    }
?>
