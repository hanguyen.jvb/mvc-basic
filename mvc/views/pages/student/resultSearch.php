<h2 style="margin-top: 30px; margin-left: 180px">Kết quả tìm kiếm</h2>
<table id="table-student">
    <tr>
        <th>Id</th>
        <th>Fullname</th>
        <th>Username</th>
        <th>Email</th>
        <th>Class</th>
        <th>Department</th>
        <th>Phone</th>
        <th>Birthday</th>
        <th>Gender</th>
    </tr>
    <tr>
        <?php
            while ($student = mysqli_fetch_array($data['searchStudent'])) { ?>
                <tr>
                    <td><?php echo $student['student_id']; ?></td>
                    <td><?php echo $student['full_name']; ?></td>
                    <td><?php echo $student['username']; ?></td>
                    <td><?php echo $student['email']; ?></td>
                    <td><?php echo $student['class_name']; ?></td>
                    <td><?php echo $student['department_name']; ?></td>
                    <td><?php echo $student['phone']; ?></td>
                    <td><?php echo $student['birthday']; ?></td>
                    <td>
                        <?php 
                            if ($student['gender'] == 1) {
                                echo "Nam";
                            } else {
                                    echo "Nữ";
                            }
                        ?>
                    </td>
                </tr>          
        <?php } ?>
    <tr>
</table>
