<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Student</title>
</head>
<body>
    <div id="add-student">
        <div class="add-student-box">
            <form class="add-student-form" method="POST" id="formAddStudent" enctype="multipart/form-data">
                <h3>Add Student</h3>
                <input type="text" name="fullname" id="fullname" placeholder="fullname">
                <input type="text" name="username" id="username" placeholder="username">
                <input type="email" name="email" id="email" placeholder="email">
                <input type="password" name="password" id="password" placeholder="password">
                <select id="class-student" name="class_student" id="class-student">
                    <?php while ($listClassOfStudent = mysqli_fetch_array($data['listClassOfStudent'])) { ?>
                        <option value="<?php echo $listClassOfStudent['class_id']; ?>">
                            <?php echo $listClassOfStudent['class_name']; ?>
                        </option>
                    <?php } ?>
                </select>
                <select id="department-student" name="department_student">
                    <?php while ($departmentOfStudent = mysqli_fetch_array($data['listDepartmentOfStudent'])) { ?>
                        <option value="<?php echo $departmentOfStudent['department_id']; ?>">
                            <?php echo $departmentOfStudent['department_name']; ?>
                        </option>
                    <?php } ?>
                </select>
                <label for="uploadAvatar" style="font-size: 19px;">Upload Avatar(just upload png, jpg, jpeg)</label>
                <input type="file" name="fileToUpload" id="fileToUpload" data-type='image'>
                <div class="empty-text">Upload images</div>
                <input type="number" name="phone" id="phone" placeholder="your phone">
                <input type="date" name="birthday" id="birthday" placeholder="your birthday">
                <div>
                    <input type="radio" id="male" name="gender" value="1"><label for="male">Male</label>
                    <input type="radio" id="female" name="gender" value="0"><label for="female">Female</label>
                </div>
                <input type="submit" name="add-student" class="btn-add">
            </form>                            
        </div>
    </div>
</body>
</html>

<script>
    $('#fileToUpload').on('change', function() {
        var file = this.files[0];
        var imagefile = file.type;
        var imageTypes = ["image/jpeg", "image/png", "image/jpg"];
        if (imageTypes.indexOf(imagefile) == -1) {
            return false;
            $(this).empty();
        } else {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(".empty-text").html('<img src="' + e.target.result + '"  />');
            };
            reader.readAsDataURL(this.files[0]);
        }
    });
</script>
