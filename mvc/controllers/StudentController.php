<?php
    class StudentController extends Controller
    {
        public function index($page)
        {
            $recordPerPage = 3;
            if (!isset($page)) {
                $page = 1;
            } else {
                $pageStudent = $page;
            }
            $perPage = ($pageStudent - 1) * $recordPerPage;
            $model = $this->model('Student');

            $this->view('master', [
                'pageStudent' => 'student/list',
                'listLimitStudent' => $model->listLimitStudent($perPage, $recordPerPage),
                'listStudent' => $model->listStudent()
            ]);
        }
        
        public function add()
        {
            if (isset($_POST['add-student'])) {
                $fullname = $_POST['fullname'];
                $username = $_POST['username'];
                $email = $_POST['email'];
                $password = $_POST['password'];
                $classOfStudent = $_POST['class_student'];
                $departmentOfStudent = $_POST['department_student'];
                $phone = $_POST['phone'];
                $birthday = $_POST['birthday'];
                $gender = $_POST['gender'];

                //upload avatar
                $avatarName = $_FILES['fileToUpload']['name'];
                $avatarSize = $_FILES['fileToUpload']['size'];
                $avatarTmp = $_FILES['fileToUpload']['tmp_name'];
                $targetDir = './image/';
                $targetFile = $targetDir.basename($avatarName);

                if ($avatarSize > 5242880) {
                    $errorUploadFile['fileToUpload'] = "";
                    echo "<script>alert('Only upload image below 5MB')</script>";
                }
                
                $fileType = pathinfo($avatarName, PATHINFO_EXTENSION);
                $fileTypesAllowed = array('png', 'jpg', 'jpeg');
                if (!in_array(strtolower($fileType), $fileTypesAllowed)) {
                    $errorUploadFile['fileToUpload'] = "";
                    echo "<script>alert('Only upload image files png, jpg, jpeg')</script>";
                }
               
                if (file_exists($targetFile)) {
                    $errorUploadFile['fileToUpload'] = "";
                    echo "<script>alert('file already exists')</script>";
                }
                
                if (empty($errorUploadFile)) {
                    $model = $this->model('Student');
                    $addStudent = $model->addStudent($fullname, $email, $phone, $birthday, $gender, $classOfStudent, $departmentOfStudent, $password, $username, $avatarName);
                    move_uploaded_file($avatarTmp, $targetFile);

                    header("location: /mvc-basic/StudentController/index/1");
                }
            }
            
            $modelClass = $this->model('ClassStudent');
            $listClassOfStudent = $modelClass->listClassOfStudent();
            $modelDepartment = $this->model('DepartmentStudent');
            $listDepartmentOfStudent = $modelDepartment->listDepartmentOfStudent();

            $this->view('master', [
                "pageAddUser" => "/student/add",
                "listClassOfStudent" => $listClassOfStudent,
                "listDepartmentOfStudent" => $listDepartmentOfStudent,
            ]);
        }

        public function edit($student_id)
        {
            if (isset($_POST['edit-student'])) {
                $student_id = $_POST['student_id'];
                $fullname = $_POST['fullname'];
                $username = $_POST['username'];
                $email = $_POST['email'];
                $password = $_POST['password'];
                $classOfStudent = $_POST['class_student'];
                $departmentOfStudent = $_POST['department_student'];
                $phone = $_POST['phone'];
                $birthday = $_POST['birthday'];
                $gender = $_POST['gender'];

                //upload avatar
                $avatarName = $_FILES['fileToUpdate']['name'];
                $avatarSize = $_FILES['fileToUpdate']['size'];
                $avatarTmp = $_FILES['fileToUpdate']['tmp_name'];
                $targetDir = './image/';
                $targetFile = $targetDir.basename($avatarName);
                
                if ($avatarSize > 5242880) {
                    $errorUploadFile['fileToUpload'] = "";
                    echo "<script>alert('Only upload image below 5MB')</script>";
                }
                
                $fileType = pathinfo($avatarName, PATHINFO_EXTENSION);
                $fileTypesAllowed = array('png', 'jpg', 'jpeg');
                if (!in_array(strtolower($fileType), $fileTypesAllowed)) {
                    $errorUploadFile['fileToUpload'] = "";
                    echo "<script>alert('Only upload image files png, jpg, jpeg')</script>";
                }
                
                if (empty($errorUploadFile)) {
                    $model = $this->model('Student');
                    $editStudent = $model->editStudent($fullname, $email, $phone, $birthday, $gender, $classOfStudent, $departmentOfStudent, $password, $username, $avatarName, $student_id);
                    move_uploaded_file($avatarTmp, $targetFile);

                    header("location: /mvc-basic/StudentController/index/1");
                }
            }

            $model = $this->model('Student');
            $listOneStudent = $model->getOneStudent($student_id);
            $this->view('master', [
                "pageEditUser" => "/student/edit",
                "listOneStudent" => $listOneStudent
            ]);
        }

        public function delete()
        {
            if (isset($_POST['delete-student'])) {
                $idCheck = $_POST['checkbox_id'];

                foreach ($idCheck as $id) {
                    $model = $this->model('Student');
                    $deleteStudent = $model->deleteStudent($id);
                }

                header('location: ../StudentController/index/1');
            }
        }

        public function search()
        {
            if (isset($_POST['search'])) {
                $keySearch = $_POST['key_search'];
                if ($keySearch == "") {
                    header("location: ../StudentController/index/1");
                } else {
                    $model = $this->model('Student');   
                    $searchStudent = $model->searchStudent($keySearch);
    
                    $this->view('master', [
                        "pageSearchUser" => "/student/resultSearch",
                        "searchStudent" => $searchStudent
                    ]);
                }
            }
        }
    }
?>
