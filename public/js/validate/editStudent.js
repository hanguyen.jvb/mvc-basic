$().ready(function () {
    $("#formEditStudent").validate({
        rules: {
            fullname: "required",
            username: "required",
            email: "required",
            password: {
                required: true,
                minlength: 6
            },
            fileToUpdate: "required",
            phone: {
                required: true,
                minlength: 10,
                maxlength: 11
            },
            birthday: {
                required: true
            },
        },
        messages: {
            fullname: "Please update your fullname",
            username: "Please update your username",
            email: "Please update your email",
            password: {
                required: "Please update your password",
                minlength: "Your password is from 6 characters"
            },
            phone: "Please choose your file",
            phone: {
                required: "Please update your phone",
                minlength: "Your phone number from 10 to 11 numbers",
                maxlength: "Your phone number from 10 to 11 numbers"
            },
            birthday: "Please update your birthday",
        }
    });
    $("#formEditStudent").validate({
        onfocusout: false
      });
})
